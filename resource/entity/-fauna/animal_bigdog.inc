
	{props "flesh" "evil_animal" "dog-new-ovcharka"}
	{obstacleID "human"}
	{PatherID	"animal"}
	{collider "animal"}
	{lodclass "human"}
	{brain "vehicle"
		{state "vehicle"}
	}
	{targetClass 	"human"}
	{targetSelector "tank_bullet"}
	{Chassis "track"
		{Locomotion
			{maxspeed 30}
			{StartTime 2}
			{TurnStart 0.2}
			{TurnTime 1}
		}
	}
	{extender "inventory"
		{box
			{Size 6 10}
			{weight 10000}
			{item "jaws ammo" 10000}
;			{item "jaws dog weapon"}
		}
	}
	{Weaponry
		{place "gun"
		}
	}
	{sensor
		{visor "main"
    		{vision
				{h_fov 270}
  			 	{v_fov 180}
				{uncover 10}
				{radius 50}
    		}
			{bone "head"}
		}
    }
 	{volume "body"
		{able {blast} {bullet} {contact}}
		{thickness 5}
	}
	{extender "vitality"
		{Health {max 5000} {current 5000}}
	}
