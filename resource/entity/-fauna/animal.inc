
	{props "flesh" "evil_animal" "with_gun"}
	{obstacleID "human"}
	{PatherID	"animal"}
	{collider "animal"}
	{lodclass "human"}
	{brain "vehicle"
		{state "vehicle"}
	}
	{targetClass 	"human"}
	{targetSelector "tank_bullet"}
	{Chassis "track"
		{Locomotion
			{maxspeed 40}
			{StartTime 2}
			{TurnStart 0.2}
			{TurnTime 1}
		}
	}
	{extender "inventory"
		{box
			{Size 6 10}
			{weight 10000}
			{item "jaws ammo" 10000}
;			{item "jaws weapon"}
		}
	}
	{Weaponry
		{place "gun"
			{rotate}
		}
	}
	{sensor
		{visor "main"
    		{vision
				{h_fov 270}
  			 	{v_fov 180}
				{uncover 10}
				{radius 50}
    		}
			{bone "head"}
		}
    }
 	{volume "body"
		{able {blast} {bullet} {contact}}
		{thickness 5}
	}
	{extender "vitality"
		{Health {max 1000} {current 1000}}
	}
